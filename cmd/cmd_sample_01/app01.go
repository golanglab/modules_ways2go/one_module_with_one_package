package main

import (
	"one_module_with_one_package/lib/lib01"
	"one_module_with_one_package/lib/lib02"
)

func main() {
	println("Hello I am app 1")
	lib01.Hello()
	lib02.Hello()
}
