package main

import (
	"one_module_with_one_package/lib/lib01"
	"one_module_with_one_package/lib/lib02"
)

func main() {
	println("Hello I am app 2")
	lib02.Hello()
	lib01.Hello()
}
