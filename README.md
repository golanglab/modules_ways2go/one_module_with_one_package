# One module with one package

Module includes only one package and one go.mod in the root, but files are stored in different subdirectories

```
.
└── one_module_with_one_package
    ├── README.md
    ├── cmd
    │   ├── cmd_sample_01
    │   │   └── app01.go
    │   └── cmd_sample_02
    │       └── app02.go
    ├── go.mod
    └── lib
        ├── lib01
        │   └── lib01_sample.go
        └── lib02
            └── lib02_sample.go
``` 